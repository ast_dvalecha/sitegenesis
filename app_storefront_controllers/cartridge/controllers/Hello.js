'use strict';

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * Renders the hello Page
 */
function world() {
    app.getView().render('WT1.2/hello');
}

exports.World = guard.ensure(['get'], world);